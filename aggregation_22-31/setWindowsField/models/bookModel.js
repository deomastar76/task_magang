const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
  judul: String,
  penulis: String,
  jumlahHalaman: Number,
  tanggalTerbit: String,
});

module.exports = mongoose.model('bookSetWindowsField', bookSchema);