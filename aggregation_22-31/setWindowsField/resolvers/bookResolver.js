const BookModel = require('../models/bookModel');

const resolvers = {
  Query: {
    setWindowFields: async (_, { mulai, akhir }) => {
      const result = await BookModel.aggregate([
        { $match: { tanggalTerbit: { $gte: mulai, $lte: akhir } } },
        { $group: { _id: null, totalItems: { $sum: 1 }, items: { $push: "$$ROOT" } } },
        {
          $project: {
            _id: 0,
            totalItems: 1,
            items: { $slice: ["$items", 0, 10] },
          },
        },
      ]);

      return result[0];
    },
  },
};

module.exports = resolvers;
