const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const typeDefs = require('./schema/bookSchema');
const bookResolvers = require('./resolvers/bookResolver');

// Konfigurasi koneksi ke MongoDB
async function startServer() {
  try {
    const url = 'mongodb://127.0.0.1:27017/buku';

    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log('Koneksi ke MongoDB berhasil.');

    const server = new ApolloServer({
      typeDefs,
      resolvers: bookResolvers,
    });

    const { url: serverUrl } = await server.listen({ port: 5000 });
    console.log(`Server running at ${serverUrl}`);
  } catch (error) {
    console.error('Terjadi kesalahan saat menghubungkan ke MongoDB:', error);
  }
}

startServer().catch((err) => {
  console.error('Terjadi kesalahan saat memulai server:', err);
});
