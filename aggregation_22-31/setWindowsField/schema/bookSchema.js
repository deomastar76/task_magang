const { gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    _id: ID!
    judul: String!
    penulis: String!
    jumlahHalaman: Int!
    tanggalTerbit: String!
  }

  type Metadata {
    totalItems: Int!
  }

  type SearchResult {
    items: [Book]
    meta: Metadata
  }

  type Query {
    setWindowFields(mulai: String!, akhir: String!): SearchResult
  }
`;

module.exports = typeDefs;
