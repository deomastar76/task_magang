// seeders/seed.js
const mongoose = require('mongoose');
const Book = require('../models/bookModel');

const URL = 'mongodb://127.0.0.1:27017/buku';

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

// Data dummy 10 buku berbeda
const dataBuku = [
    {
      judul: 'Judul Buku 1',
      penulis: 'Penulis 1',
      jumlahHalaman: 300,
      tanggalTerbit: '2022-12-15',
    },
    {
      judul: 'Judul Buku 2',
      penulis: 'Penulis 2',
      jumlahHalaman: 250,
      tanggalTerbit: '2023-02-28',
    },
    {
      judul: 'Judul Buku 3',
      penulis: 'Penulis 3',
      jumlahHalaman: 400,
      tanggalTerbit: '2023-01-05',
    },
    {
      judul: 'Judul Buku 4',
      penulis: 'Penulis 4',
      jumlahHalaman: 180,
      tanggalTerbit: '2021-09-20',
    },
    {
      judul: 'Judul Buku 5',
      penulis: 'Penulis 1',
      jumlahHalaman: 320,
      tanggalTerbit: '2022-11-10',
    },
    {
      judul: 'Judul Buku 6',
      penulis: 'Penulis 5',
      jumlahHalaman: 280,
      tanggalTerbit: '2023-04-17',
    },
    {
      judul: 'Judul Buku 7',
      penulis: 'Penulis 2',
      jumlahHalaman: 220,
      tanggalTerbit: '2023-03-12',
    },
    {
      judul: 'Judul Buku 8',
      penulis: 'Penulis 6',
      jumlahHalaman: 150,
      tanggalTerbit: '2021-12-01',
    },
    {
      judul: 'Judul Buku 9',
      penulis: 'Penulis 3',
      jumlahHalaman: 380,
      tanggalTerbit: '2023-06-23',
    },
    {
      judul: 'Judul Buku 10',
      penulis: 'Penulis 7',
      jumlahHalaman: 270,
      tanggalTerbit: '2022-10-08',
    },
  ];
  

async function seedData() {
  try {
    await Book.deleteMany({});
    const insertedData = await Book.insertMany(dataBuku);
    console.log('Seeding data berhasil:', insertedData);
  } catch (error) {
    console.error('Terjadi kesalahan saat seeding data:', error);
  } finally {
    mongoose.disconnect();
  }
}

seedData();
