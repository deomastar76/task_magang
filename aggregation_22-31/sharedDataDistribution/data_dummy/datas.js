// seeders/seed.js
const mongoose = require('mongoose');
const Book = require('../models/komikModel');

const URL = 'mongodb://127.0.0.1:27017/buku';

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

// Data dummy 10 buku berbeda
const dataBuku = [
    {
      name: 'Aaruto',
      category: 'Fantasy',
      price: 25000,
      stok: 10
    },
    {
      name: 'Baruto Volume 2',
      category: 'Fantasy',
      price: 27500,
      stok: 8
    },
    {
      name: 'Caikyu Volume 2',
      category: 'Sport, Volly',
      price: 30000,
      stok: 6
    },
    {
      name: 'Dsubasa Volume 1',
      category: 'Sport',
      price: 23000,
      stok: 4
    },
    {
      name: 'Fsubasa Volume 2',
      category: 'Sport',
      price: 25000,
      stok: 2
    },
  ];
  

async function seedData() {
  try {
    await Book.deleteMany({});
    const insertedData = await Book.insertMany(dataBuku);
    console.log('Seeding data berhasil:', insertedData);
  } catch (error) {
    console.error('Terjadi kesalahan saat seeding data:', error);
  } finally {
    mongoose.disconnect();
  }
}

seedData();
