const { gql } = require('apollo-server');

const typeDefs = gql`
  type Data {
    _id: ID!
    name: String!
    category: String!
    price: Float!
    stok: Int!
  }
  
  type CategoryTotal {
    category: String!
    total: Float!
  }
  
  type Query {
    getDataById(id: ID!): Data
    getAllData: [Data]
    getTotalByCategory(skip: Int = 0): [CategoryTotal]
    getDataSortedByName: [Data]
    getDataSortedByCount: [Data]
  }
`;

module.exports = typeDefs;
