const DataModel = require('../models/komikModel');

const resolvers = {
  Query: {
    getDataById: async (_, { id }) => {
      return await DataModel.findById(id);
    },
    getAllData: async () => {
      return await DataModel.find();
    },
    getTotalByCategory: async (_, { skip }) => {
      try {
        const result = await DataModel.aggregate([
          {
            $group: {
              _id: '$category',
              total: { $sum: '$price' },
            },
          },
          {
            $skip: skip || 0,
          },
        ]);

        if (result && result.length > 0) {
          const categoryTotalList = result.map((item) => ({
            category: item._id,
            total: item.total,
          }));
          return categoryTotalList;
        }

        return [];
      } catch (error) {
        console.error('Error performing aggregation:', error);
        throw new Error('Failed to get total by category');
      }
    },
    getDataSortedByName: async () => {
      try {
        const result = await DataModel.find().sort({ name: 1 });

        if (result && result.length > 0) {
          return result;
        }

        return [];
      } catch (error) {
        console.error('Error fetching data:', error);
        throw new Error('Failed to get data sorted by name');
      }
    },
    getDataSortedByCount: async () => {
      try {
        const result = await DataModel.find().sort({ stok: -1 });

        if (result && result.length > 0) {
          return result;
        }

        return [];
      } catch (error) {
        console.error('Error fetching data:', error);
        throw new Error('Failed to get data sorted by stok');
      }
    },
  },
};

module.exports = resolvers;
