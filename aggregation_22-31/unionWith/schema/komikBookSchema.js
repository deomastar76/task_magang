// schema.js
const { gql } = require('apollo-server');

const typeDefs = gql`
  type Komik {
    _id: ID!
    name: String!
    category: String!
    price: Float!
    stok: Int!
  }

  type Book {
    _id: ID!
    title: String!
    author: String!
    genre: String!
  }

  union SearchResult = Komik | Book

  type Query {
    searchItems(keyword: String!): [SearchResult]
  }
`;

module.exports = typeDefs;
