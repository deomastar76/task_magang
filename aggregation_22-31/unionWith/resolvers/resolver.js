// resolvers.js
const KomikModel = require('../models/komikModel');
const BookModel = require('../models/bookModel');

const resolvers = {
    Query: {
      searchItems: async (_, { keyword }) => {
        try {
          const komikResults = await KomikModel.find({
            $or: [
              { name: { $regex: keyword, $options: 'i' } },
              { category: { $regex: keyword, $options: 'i' } },
            ],
          });
  
          const BookResults = await BookModel.find({
              $or: [
                  { title: { $regex: keyword, $options: 'i' } },
                  { genre: { $regex: keyword, $options: 'i' } },
                  ],
          });
  
          const mergedResults = [...komikResults, ...BookResults];
  
          return mergedResults;
        } catch (error) {
          console.error('Error fetching search results:', error);
          throw new Error('Failed to fetch search results');
        }
      },
    },
    SearchResult: {
      __resolveType(obj) {
        if (obj.category) {
          return 'Komik';
        }
        if (obj.genre) {
          return 'Book';
        }
        return null;
      },
    },
  };

module.exports = resolvers;
