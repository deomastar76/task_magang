// models/komikModel.js
const mongoose = require('mongoose');

const komikSchema = new mongoose.Schema({
  name: String,
  category: String,
  price: Number,
  stok: Number,
});

module.exports = mongoose.model('komik', komikSchema);
