const { gql } = require('apollo-server');

const typeDefs = gql`
  type Mahasiswa {
    id: ID!
    nama: String!
    nim: String!
    jurusan: String!
  }

  type Query {
    mahasiswas(searchData: String!): [Mahasiswa]
  }
`;

module.exports = typeDefs;
