const Mahasiswa = require('../models/mahasiswa');

const resolvers = {
  Query: {
    mahasiswas: async (_, { searchData }) => {
      try {
        const searchDatas = { $regex: searchData, $options: 'i' };
        const mahasiswas = await Mahasiswa.aggregate([
          {
            $match: {
              $or: [
                { nama: searchDatas },
                { nim: searchDatas },
              ],
            },
          },
          {
            $addFields: {
              id: { $toString: '$_id' },
            },
          },
        ]);
        return mahasiswas;
      } catch (error) {
        console.error('Terjadi kesalahan saat mencari data:', error);
        throw new Error('Gagal mencari data.');
      }
    },
  },
};

module.exports = resolvers;
