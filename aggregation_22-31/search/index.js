const mongoose = require('mongoose');
const { ApolloServer }  = require('apollo-server');
const typeDefs = require('./schema/mahasiswaSchema');
const resolvers = require('./resolvers/mahasiswaResolver');

async function startServer() {
  try {
    const url = 'mongodb://127.0.0.1:27017/data_mahasiswa';

    
    const server = new ApolloServer({
      typeDefs,
      resolvers,
    });

    mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(() => {
      console.log("MongoDB Connected");
      return server.listen({port: 5000});
    })
    .then((res) => {
        console.log(`Server running at ${res.url}`)
    });

    console.log('Koneksi ke MongoDB berhasil.');
  } catch (error) {
    console.error('Terjadi kesalahan saat menghubungkan ke MongoDB:', error);
  }
}

startServer().catch((err) => {
  console.error('Terjadi kesalahan saat memulai server:', err);
});
