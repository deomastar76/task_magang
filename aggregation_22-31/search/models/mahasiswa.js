const mongoose = require('mongoose');

const mahasiswaSchema = new mongoose.Schema({
  nama: String,
  nim: String,
  jurusan: String,
});

module.exports = mongoose.model('mahasiswas', mahasiswaSchema);
