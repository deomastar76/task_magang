  // seeders/seed.js
  const mongoose = require('mongoose');
  const Book = require('../models/bookModel');

  // Ganti dengan URI MongoDB Anda dan nama database Anda
  const URL = 'mongodb://127.0.0.1:27017/buku';

  mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

  // Data dummy 10 buku berbeda
  const dataBuku = [
    {
      title: 'Judul Buku 1',
      authors: ['Penulis 1', 'Penulis 2', 'Penulis 3'],
      genre: 'Fiksi',
      isPublished: true,
    },
    {
      title: 'Judul Buku 2',
      authors: ['Penulis 2', 'Penulis 3'],
      genre: 'Non-Fiksi',
      isPublished: false,
    },
    {
      title: 'Judul Buku 3',
      authors: ['Penulis 1', 'Penulis 3', 'Penulis 4'],
      genre: 'Fiksi',
      isPublished: true,
    },
    {
      title: 'Judul Buku 4',
      authors: ['Penulis 4'],
      genre: 'Drama',
      isPublished: false,
    },
    {
      title: 'Judul Buku 5',
      authors: ['Penulis 1'],
      genre: 'Romance',
      isPublished: true,
    },
    {
      title: 'Judul Buku 6',
      authors: ['Penulis 5'],
      genre: 'Thriller',
      isPublished: true,
    },
    {
      title: 'Judul Buku 7',
      authors: ['Penulis 2'],
      genre: 'Misteri',
      isPublished: false,
    },
    {
      title: 'Judul Buku 8',
      authors: ['Penulis 6'],
      genre: 'Horor',
      isPublished: true,
    },
    {
      title: 'Judul Buku 9',
      authors: ['Penulis 3'],
      genre: 'Fantasi',
      isPublished: false,
    },
    {
      title: 'Judul Buku 10',
      authors: ['Penulis 7'],
      genre: 'Fiksi',
      isPublished: true,
    },
  ];
  
  async function seedData() {
    try {
      await Book.deleteMany({});
      const insertedData = await Book.insertMany(dataBuku);
      console.log('Seeding data berhasil:', insertedData);
    } catch (error) {
      console.error('Terjadi kesalahan saat seeding data:', error);
    } finally {
      mongoose.disconnect();
    }
  }

  seedData();
