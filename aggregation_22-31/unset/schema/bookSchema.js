const { gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    _id: ID!
    title: String!
    author: String!
    genre: String!
    isPublished: Boolean
  }

  type Query {
    getAllBooks: [Book]
  }

  type Mutation {
    unsetBookPublishedStatus(bookId: ID!): Book
  }
`;

module.exports = typeDefs;
