const BookModel = require('../models/bookModel');

const resolvers = {
  Query: {
    getAllBooks: async () => {
      return await BookModel.find();
    },
  },
  Mutation: {
    unsetBookPublishedStatus: async (_, { bookId }) => {
      try {
        const book = await BookModel.findByIdAndUpdate(
          bookId,
          { $unset: { isPublished: 1 } },
          { new: true }
        );
        return book;
      } catch (error) {
        console.error('Terdapat kesalahan pada saat mengunset published:', error);
        throw new Error('Terdapat kesalahan pada saat mengunset published');
      }
    },
  },
};

module.exports = resolvers;
