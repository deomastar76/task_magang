const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
  title: String,
  author: String,
  genre: String,
  isPublished: Boolean,
});

module.exports = mongoose.model('BookModel', bookSchema);
