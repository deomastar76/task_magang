const { gql } = require('apollo-server');

const typeDefs = gql`
    type Desc {
        _id: ID!,
        desc: String! 
    }

    type Books {
        _id: ID!,
        title: String,
        author: String,
        rating: Int
    }

    type Chairs {
        _id: ID!,
        jenis: String,
        merk: String,
        kualitas: String
    }

    type FacetResult {
        _id: String!
        count: Int!
    }
    
    type Query {
        getAllDatas: [Desc]!,
        getDesc(_id: ID!): Desc,
        facetBooks: [FacetResult]!,
        limitChairs: [Chairs]!,
        mergeChairs: String,
        outBook : String,
        redactBook(redactTitle: String) : [Books]
        replaceRootBook: [Books],
        sampleBooks: [Books]
    }

    type Mutation  {
        createDesc(
            desc: String!
        ): Desc!,

        

        createBooks(
            title: String!,
            author: String!,
            rating: Int
        ): Books!,

        createChairs(
            jenis: String!,
            merk: String!,
            kualitas: String!
        ): Chairs!,
        
        updateDesc(
            _id: ID!,
            desc: String 
        ): Desc!,
        deleteDesc(_id: ID!):Boolean
    }
`;

module.exports = typeDefs;