const DescModel = require('./models/Desc');
const BooksModel = require('./models/Books');
const ChairsModel = require('./models/Chairs');

module.exports = {
    Query: {
        getAllDatas: async () => await DescModel.find({}),
        getDesc: async (_, args) => await DescModel.findById(args._id),
        facetBooks: async () => {
          try {
            const books = await BooksModel.find({}); // Find books based on any criteria, e.g., title
    
            const aggregationResult = await BooksModel.aggregate([
              {
                $match: { _id: { $in: books.map(book => book._id) } } // Match only the books found in the previous step
              },
              {
                $facet: {
                  genresCount: [
                    {
                      $group: {
                        _id: "$author",
                        count: { $sum: 1 }
                      }
                    },
                    {
                      $sort: { count: -1 }
                    }
                  ],
                }
              }
            ]);
            return aggregationResult[0].genresCount;
          } catch (err) {
            // Handle error
            console.error(err);
            throw err;
          }
        },
      limitChairs: async () => {
        try {
          const limitValue = 2; // Atur jumlah data yang ingin Anda batasi di sini (misalnya, 10)
      
          const aggregationResult = await ChairsModel.aggregate([
            {
              $limit: limitValue // Menggunakan operasi $limit untuk membatasi jumlah data yang dikembalikan
            }
          ]);
      
          return aggregationResult;
        } catch (err) {
          // Handle error
          console.error(err);
          throw err;
        }
      },

      mergeChairs: async () => {
        try {

          const pipeline = [
            { $merge: { into: "chairs" } }
          ];
      
          await BooksModel.aggregate(pipeline).exec(); 
          return 'Operasi $merge berhasil dilakukan.';
        } catch (error) {
          console.error('Terjadi kesalahan:', error);
          throw new Error('Gagal melakukan operasi $merge.');
        }
      },   

      outBook: async () => {
        try {
          const pipeline = [
            // Tahap agregasi untuk melakukan perhitungan atau transformasi data
            // Misalnya:
            {
              $group: {
                _id: {
                  title: "$title",
                  rating: "$rating",
                  author: "$author"
                },
                total: { $sum: "$jumlah" }
              }
            },
      
            // Tahap akhir untuk menyimpan hasil aggregasi ke dalam koleksi baru
            { $out: "hasil_agregasi" } // Ganti "hasil_agregasi" dengan nama koleksi baru yang Anda inginkan
          ];

          await BooksModel.aggregate(pipeline).exec(); 

          console.log("Operasi agregasi $out selesai.");
      

        } catch (e) {
          console.error("Terjadi kesalahan:", e);
        }
    },   
    
    redactBook: async (redactTitle) => {
      try {
        // Melakukan query ke database MongoDB
        const redactedBooks = await BooksModel.aggregate([
          {
            $match: { title: { $ne: redactTitle } } // Menggunakan $ne (not equal) untuk menghilangkan buku dengan judul yang di-redact
          }
        ]);
    
        console.log("Operasi redact selesai.");
    
        return redactedBooks; // Mengembalikan hasil redact
      } catch (e) {
        console.error("Terjadi kesalahan:", e);
        return "Terjadi kesalahan saat melakukan redact.";
      }
    },

    replaceRootBook: async () => {
      try {
        // Melakukan query ke database MongoDB
        const books = await BooksModel.aggregate([
          {
            $project: {
              _id: 0, // Tidak termasuk field _id di hasil akhir
              info: { $concat: ["$title", " (", { $toString: "$rating" }, ")", " by ", "$author"] },
              rating: 1, // Menyertakan field rating untuk dijadikan field baru di hasil akhir
              author: 1, // Menyertakan field author untuk dijadikan field baru di hasil akhir
            },
          },
          {
            $replaceRoot: {
              newRoot: {
                info: "$info",
                rating: "$rating", // Mengganti struktur root dengan field info, rating, dan author
                author: "$author",
                
              },
            },
          },
        ]);
    
        console.log("Operasi replaceRoot selesai.");
        console.log("Hasil books:", books);
    
        return books; // Mengembalikan hasil replaceRoot
      } catch (e) {
        console.error("Terjadi kesalahan:", e);
        return "Terjadi kesalahan saat melakukan replaceRoot.";
      }
    },

    sampleBooks: async () => {
      try {
        // Melakukan query ke database MongoDB
        const books = await BooksModel.aggregate([
          {
            $sample: {
              size: 5 // Mengambil 5 dokumen acak dari koleksi BooksModel
            }
          },
          {
            $project: {
              _id: 0, // Tidak termasuk field _id di hasil akhir
              title: 1, // Menyertakan field title
              author: 1, // Menyertakan field author
              rating: 1 // Menyertakan field rating
            }
          }
        ]);
    
        console.log("Operasi sampleBooks selesai.");
    
        return books; // Mengembalikan hasil sampleBooks
      } catch (e) {
        console.error("Terjadi kesalahan:", e);
        return "Terjadi kesalahan saat melakukan sampleBooks.";
      }
    },


    
  },



    Mutation: {
        createDesc: async (_, args) => {
            const desc = new DescModel(args);
            await desc.save();
            return desc;
        },
        createBooks: async (_, args) => {
        try {
            const { title, author, rating } = args;
            const newBook = new BooksModel({
            title: title,
            author: author,
            rating: rating
            });
            const savedBook = await newBook.save();
            return savedBook;
        } catch (error) {
            throw new Error('Failed to create a new book.');
        }
        },
        createChairs: async (_, args) => {
        try {
          const { jenis, merk, kualitas } = args;
          const newChairs = new ChairsModel({
          jenis: jenis,
          merk: merk,
          kualitas: kualitas
          });
          const savedChairs = await newChairs.save();
          return savedChairs;
      } catch (error) {
          throw new Error('Failed to create a new chairs.');
      }
      },



        updateDesc: async (_, args) => {
            const desc = await DescModel.findByIdAndUpdate(args._id, args, {new: true});
            return desc;
        },

        deleteDesc: async (_, args) => {
            const desc = await DescModel.findByIdAndRemove(args._id);
            if(desc) return true;
            return false;
        }

    }
}