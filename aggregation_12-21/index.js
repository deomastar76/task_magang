const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
// Connection to mongoDB
mongoose.connect('mongodb+srv://deomastar76:V0WtNSup2eRnzhaE@cluster0.lnq5buj.mongodb.net/todo', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});


const server = new ApolloServer({ typeDefs, resolvers });
server.listen().then(({ url }) => {
    console.log(`Server listening on ${url}`);
});
