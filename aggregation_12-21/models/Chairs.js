const mongoose = require('mongoose');

const ChairsSchema = new mongoose.Schema({
    jenis: String,
    merk: String,
    kualitas: String,

}, { versionKey: false });

const Chairs = mongoose.model('chairs', ChairsSchema);

module.exports = Chairs;