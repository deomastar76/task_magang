const mongoose = require('mongoose');

const BooksSchema = new mongoose.Schema({
    title: String,
    author: String,
    rating: Number,

}, { versionKey: false });

const Books = mongoose.model('books', BooksSchema);

module.exports = Books;