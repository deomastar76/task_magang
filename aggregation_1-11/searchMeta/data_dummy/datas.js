// seeders/seed.js
const mongoose = require('mongoose');
const Book = require('../models/bookModel');

// Ganti dengan URI MongoDB Anda dan nama database Anda
const URL = 'mongodb+srv://dbtask4:auz1BKQMUeZ9xgOl@cluster0.yo9m7e3.mongodb.net/buku';

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

// Data dummy 10 buku berbeda
const dataBuku = [
  {
    title: 'Judul Buku 1',
    author: 'Penulis 1',
    genre: 'Fiksi',
  },
  {
    title: 'Judul Buku 2',
    author: 'Penulis 2',
    genre: 'Non-Fiksi',
  },
  {
    title: 'Judul Buku 3',
    author: 'Penulis 3',
    genre: 'Fiksi',
  },
  {
    title: 'Judul Buku 4',
    author: 'Penulis 4',
    genre: 'Drama',
  },
  {
    title: 'Judul Buku 5',
    author: 'Penulis 1',
    genre: 'Romance',
  },
  {
    title: 'Judul Buku 6',
    author: 'Penulis 5',
    genre: 'Thriller',
  },
  {
    title: 'Judul Buku 7',
    author: 'Penulis 2',
    genre: 'Misteri',
  },
  {
    title: 'Judul Buku 8',
    author: 'Penulis 6',
    genre: 'Horor',
  },
  {
    title: 'Judul Buku 9',
    author: 'Penulis 3',
    genre: 'Fantasi',
  },
  {
    title: 'Judul Buku 10',
    author: 'Penulis 7',
    genre: 'Fiksi',
  },
];

async function seedData() {
  try {
    await Book.deleteMany({});
    const insertedData = await Book.insertMany(dataBuku);
    console.log('Seeding data berhasil:', insertedData);
  } catch (error) {
    console.error('Terjadi kesalahan saat seeding data:', error);
  } finally {
    mongoose.disconnect();
  }
}

seedData();
