const { gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    _id: ID!
    title: String!
    author: String!
    genre: String!
  }

  type Metadata {
    totalItems: Int!
  }

  type SearchResult {
    items: [Book]
    meta: Metadata
  }

  type Query {
    searchBooks(query: String!): SearchResult
  }
`;

module.exports = typeDefs;
