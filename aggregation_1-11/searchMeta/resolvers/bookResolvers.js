const BookModel = require('../models/bookModel');

const bookResolver = {
  Query: {
    searchBooks: async (_, { query }) => {
      try {
        // Lakukan pencarian menggunakan aggregation MongoDB
        const result = await BookModel.aggregate([
          {
            $search: {
              index: 'default',
              text: {
                query: query,
                path: 'title', // Cari di bidang "title"
              },
            },
          },
        ]);

        // Hitung jumlah total item yang ditemukan
        const totalItems = result.length;
        
        // Hasilkan objek SearchResult
        const searchResult = {
          items: result,
          meta: {
            totalItems,
          },
        };

        return searchResult;
      } catch (error) {
        // Tangani kesalahan dengan menampilkan pesan kesalahan
        console.error('Terjadi kesalahan saat melakukan pencarian:', error);
        throw new Error('Gagal melakukan pencarian buku.');
      }
    },
  },
};
module.exports = bookResolver;